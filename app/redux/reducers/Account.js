/*
state: {
    name: string
    address: string
    balance: number
}
*/

const initialState = {}

const account = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_ACCOUNT':
            return {
                ...state,
                ...action
            }
        case 'UPDATE_BALANCE':
            return {
                ...state,
                balance: action.balance
            }
        default:
            return state
    }
}

export default account