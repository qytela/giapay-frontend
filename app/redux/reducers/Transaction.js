/*
data: [
    {
        id: number
        type: string
        miles: string
        receipt: object
    }
]
*/

const initialState = {
    data: []
}

const transaction = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_TRANSACTION':
            return {
                ...state,
                data: [...state.data, action.data]
            }
        default:
            return state
    }
}

export default transaction