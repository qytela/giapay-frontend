import { combineReducers } from 'redux'

import account from './Account'
import transaction from './Transaction'

export default combineReducers({
    account,
    transaction
})