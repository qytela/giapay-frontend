import React, { Component } from 'react'
import { View, Text, TextInput, Dimensions, ScrollView, StatusBar, SafeAreaView, TouchableOpacity } from 'react-native'
import { Container } from 'native-base'
import { connect } from 'react-redux'
import moment from 'moment-timezone'

import { numberFormat }  from '../../helpers'

const { width, height } = Dimensions.get('window')

class TopUp extends Component {
    constructor(props) {
        super(props)
        this.state = {
            miles: 0
        }
    }

    onTopUpPress = () => {
        const { miles } = this.state

        this.props.navigation.navigate({
            key: 'TransactionPaymentSelect',
            routeName: 'TransactionPaymentSelect',
            params: {
                miles
            }
        })
    }

    render() {
        const { balance } = this.props.account

        return (
            <Container style={{ backgroundColor: '#232323' }}>
                <StatusBar
                    translucent
                    backgroundColor='transparent'
                />
                <SafeAreaView style={{ marginTop: StatusBar.currentHeight }}>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={{ padding: 15 }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 25, fontWeight: '500', color: 'white' }}>Top Up Miles</Text>
                            </View>
                            <View style={{ alignItems: 'baseline', marginTop: 50 }}>
                                <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white' }}>Your Balance Miles is</Text>
                                <Text style={{ fontSize: 30, fontWeight: '500', color: '#EED064', marginTop: 5 }}>{numberFormat(parseInt(balance))}</Text>
                                <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', marginTop: 5 }}>Enter either Miles or IDR</Text>
                                <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', marginTop: 15 }}>Miles</Text>
                                <TextInput
                                    keyboardType='number-pad'
                                    onChangeText={(miles) => this.setState({ miles })}
                                    style={{ width: '100%', height: width / 8, backgroundColor: 'white', borderRadius: 10, marginTop: 5, fontSize: 20, fontWeight: '500', color: '#232323' }}
                                />
                                <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', marginTop: 15 }}>IDR</Text>
                                <TextInput
                                    keyboardType='number-pad'
                                    style={{ width: '100%', height: width / 8, backgroundColor: 'white', borderRadius: 10, marginTop: 5, fontSize: 20, fontWeight: '500', color: '#232323' }}
                                />
                            </View>
                            <View style={{ alignItems: 'flex-end' }}>
                                <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', marginTop: 15 }}>1 Miles approximately IDR 500</Text>
                            </View>
                            <TouchableOpacity
                                onPress={() => this.onTopUpPress()}
                                activeOpacity={0.8}
                                style={{ width: '100%', height: width / 8, alignItems: 'center', justifyContent: 'center', backgroundColor: '#EED064', borderRadius: 10, marginTop: 50 }}
                            >
                                <Text style={{ fontSize: 15, fontWeight: '500', color: '#232323' }}>Top Up</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        account: state.account,
        transaction: state.transaction
    }
}

export default connect(mapStateToProps)(TopUp)
