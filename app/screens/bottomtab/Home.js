import React, { Component } from 'react'
import { View, Text, Image, ImageBackground, Dimensions, ScrollView, StatusBar, SafeAreaView, TouchableWithoutFeedback, ProgressBarAndroid } from 'react-native'
import { Container, Icon } from 'native-base'
import { connect } from 'react-redux'

import { numberFormat }  from '../../helpers'

const { width, height } = Dimensions.get('window')

class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [
                {
                    id: 0,
                    title: 'Stay with Mulia Hotels and Earn Miles',
                    subTitle: 'Earn 2x Miles and Dining benefit',
                    type: 'earn',
                    image: 'https://www.themulia.com/getmedia/e3eec84b-3ce1-4a8b-851e-c8cdd47f16f0/slider-thesuites01.jpg/?width=1440&height=578&ext=.jpg&maxsidesize=640',
                    price: 7000,
                    detail: {
                        earn_miles: {
                            weekend: '3x Miles',
                            weekday: '2x Miles'
                        },
                        overview: [
                            'Enjoy a Hotel Mulia. Get that holiday feeling without ever setting foot in an aiport. Enjoy a luxurious staycation at selected Muliat Hotels in Jakarta.',
                            'Book a "Staycation with Double miles" package to stay in a spacious room, with special perks including breakfast and dinner at popular buffet restaurants in town and access to amazing leisure facilities, as well as 2x Miles for your stay.'
                        ],
                        tc: [
                            '1. This offer is valid for stays booked and completed between 1 January 2019 and 20 March 2020 at Mulia Hotels.'
                        ]
                    }
                },
                {
                    id: 1,
                    title: 'ONE OKE ROCK Road To Jakarta',
                    subTitle: 'Reedem voucher now',
                    type: 'reedem',
                    image: 'https://img.okeinfo.net/content/2020/01/13/205/2152459/one-ok-rock-gelar-konser-di-jakarta-jM1wxvb2yc.png',
                    price: 5000,
                    detail: {
                        reedemption_period: {
                            from: '21 Jan 2020',
                            to: '15 Mar 2020'
                        },
                        delivery_period: {
                            from: '19 Mar 2020',
                            to: '21 Mar 2020'
                        },
                        overview: [
                            'Performance date (Time)',
                            '3 April 2020 (Fri) 8:15 PM'
                        ],
                        venue: [
                            'National Stadium in Singapore',
                            'Address: 1 Stadium Dr, Singapore 123456'
                        ],
                        tc: [
                            '1. Either the member or the member\'s representative must be present to receive the tickets at the agreed date and time.The member or representative is required to sign an acknowledgement slip upon receipt of tickets.',
                            '2. Redemption includes a one-off delivery within the designated areas (excluding PO Box addresses, outlying islands and restricted areas in Hong Kong). No delivery or self-collection services are available in other locations.',
                            '3. Members must be provide a valid contract phone number and mailing address for delivery purposes.',
                            '4. 1 Account 1 Reedem.'
                        ]
                    }
                }
            ]
        }
    }

    componentDidMount() {
        this.props.dispatch({ type: 'SET_ACCOUNT', name: 'Fansa', address: '0x7dd54E2098dCAD889c966A99cc064Ba84F910B87', balance: 100000 })
    }

    renderOffers = () => {
        const { data } = this.state

        return data.map((item, index) => {
            const { id, title, subTitle, type, image } = item

            return (
                <TouchableWithoutFeedback
                    key={index}
                    onPress={() => this.renderButtonOffers(type, index)}
                >
                    <View style={{ padding: 15, width: width / 1.7 }}>
                        <ImageBackground source={{ uri: image }} style={{ width: width / 2, height: width / 2.5, padding: 10 }} imageStyle={{ borderRadius: 10 }}>
                            {this.renderTypeOffers(type)}
                        </ImageBackground>
                        <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', textAlign: 'left', marginTop: 5 }}>{title}</Text>
                        <Text style={{ fontSize: 13, fontWeight: 'normal', color: 'white', textAlign: 'left', marginTop: 5 }}>{subTitle}</Text>
                    </View>
                </TouchableWithoutFeedback>
            )
        })
    }

    renderButtonOffers = (type, index = 0) => {
        const { data } = this.state

        if (type == 'earn') {
            this.props.navigation.navigate({
                key: 'BookDetail',
                routeName: 'BookDetail',
                params: {
                    data: data[index]
                }
            })
        } else if (type == 'reedem') {
            this.props.navigation.navigate({
                key: 'ReedemDetail',
                routeName: 'ReedemDetail',
                params: {
                    data: data[index]
                }
            })
        }
    }

    renderTypeOffers = (type) => {
        var $type = null

        if (type == 'earn') {
            $type = <View style={{ width: width / 8, height: width / 8, borderRadius: 30, alignItems: 'center', justifyContent: 'center', backgroundColor: '#EED064' }}><Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white' }}>Earn</Text></View>
        } else if (type == 'reedem') {
            $type = <View style={{ width: width / 8, height: width / 8, borderRadius: 30, alignItems: 'center', justifyContent: 'center', backgroundColor: '#232323' }}><Text style={{ fontSize: 10, fontWeight: 'normal', color: 'white' }}>Reedem</Text></View>
        }

        return $type
    }

    render() {
        const { name, balance } = this.props.account

        return (
            <Container style={{ backgroundColor: '#232323' }}>
                <StatusBar
                    translucent
                    backgroundColor='transparent'
                />
                <SafeAreaView style={{ marginTop: StatusBar.currentHeight }}>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={{ padding: 15, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View>
                                <Text style={{ fontSize: 25, fontWeight: '500', color: 'white' }}>Hi, {name}</Text>
                                <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white' }}>Where's your next destination?</Text>
                            </View>
                            <Icon type='Ionicons' name='md-notifications' style={{ fontSize: 30, color: 'white' }} />
                        </View>
                        <View style={{ padding: 15 }}>
                            <View style={{ width: '100%', height: width / 1.8, backgroundColor: '#EED064', borderRadius: 10, padding: 15 }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Image source={require('../../images/image-giapay-black.png')} style={{ width: 130, height: 30 }} />
                                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                        <Icon type='AntDesign' name='pluscircle' style={{ fontSize: 24, color: '#232323' }} onPress={() => this.props.navigation.navigate('TopUp')} />
                                        <Text style={{ fontSize: 15, fontWeight: '500', color: '#232323' }}>TopUp</Text>
                                    </View>
                                </View>
                                <Text style={{ fontSize: 15, fontWeight: '500', color: '#232323' }}>Balance</Text>
                                <Text style={{ fontSize: 30, fontWeight: '500', color: '#232323' }}>{numberFormat(parseInt(balance))}</Text>
                                <Text style={{ fontSize: 15, fontWeight: '500', color: '#232323' }}>Miles</Text>
                                <View style={{ marginTop: 15 }}>
                                    <Text style={{ fontSize: 15, fontWeight: '500', color: '#232323' }}>GarudaMiles Silver</Text>
                                    <ProgressBarAndroid styleAttr='Horizontal' progress={0.5} indeterminate={false} color='#232323' />
                                    <Text style={{ fontSize: 10, fontWeight: '500', color: '#232323' }}>15.000 Miles to Gold</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ padding: 15, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: 20, fontWeight: '500', color: 'white' }}>Offers for you</Text>
                            <Text style={{ fontSize: 15, fontWeight: '500', color: 'white' }}>See all</Text>
                        </View>
                        <ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                        >
                            {this.renderOffers()}
                        </ScrollView>
                        <View style={{ padding: 15, flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={{ fontSize: 20, fontWeight: '500', color: 'white' }}>Grab & Enjoy</Text>
                            <Text style={{ fontSize: 15, fontWeight: '500', color: 'white' }}>See all</Text>
                        </View>
                        <ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                        >
                            {
                                this.state.data.map((item, index) => (
                                    <View  key={index} style={{ padding: 15, width: width / 1.7 }}>
                                        <ImageBackground source={{ uri: item.image }} style={{ width: width / 2, height: width / 2.5, padding: 10 }} imageStyle={{ borderRadius: 10 }}>
                                            
                                        </ImageBackground>
                                        <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', textAlign: 'left', marginTop: 5 }}>{item.title}</Text>
                                    </View>
                                ))
                            }
                            {/* <View style={{ padding: 15, width: width / 1.7 }}>
                                <ImageBackground source={{ uri: 'https://wallpaperaccess.com/full/44729.jpg' }} style={{ width: width / 2, height: width / 2.5, padding: 10 }} imageStyle={{ borderRadius: 10 }}>
                                    
                                </ImageBackground>
                                <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', textAlign: 'left', marginTop: 5 }}>Lorem ipsum is lorem lorem is lorem lorem is lorem</Text>
                            </View>
                            <View style={{ padding: 15, width: width / 1.7 }}>
                                <ImageBackground source={{ uri: 'https://wallpaperaccess.com/full/44729.jpg' }} style={{ width: width / 2, height: width / 2.5, padding: 10 }} imageStyle={{ borderRadius: 10 }}>

                                </ImageBackground>
                                <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', textAlign: 'left', marginTop: 5 }}>Lorem ipsum is lorem lorem is lorem lorem is lorem</Text>
                            </View>
                            <View style={{ padding: 15, width: width / 1.7 }}>
                                <ImageBackground source={{ uri: 'https://wallpaperaccess.com/full/44729.jpg' }} style={{ width: width / 2, height: width / 2.5, padding: 10 }} imageStyle={{ borderRadius: 10 }}>

                                </ImageBackground>
                                <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', textAlign: 'left', marginTop: 5 }}>Lorem ipsum is lorem lorem is lorem lorem is lorem</Text>
                            </View> */}
                        </ScrollView>
                    </ScrollView>
                </SafeAreaView>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        account: state.account,
        transaction: state.transaction
    }
}

export default connect(mapStateToProps)(Home)
