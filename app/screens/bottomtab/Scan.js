import React, { Component } from 'react';
import { View } from 'react-native';
import { CameraKitCameraScreen } from 'react-native-camera-kit'

class Scan extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    onReadCode = (text) => {
        if (text !== '') {
            this.props.navigation.navigate({
                key: 'TransactionSent',
                routeName: 'TransactionSent'
            })
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <CameraKitCameraScreen
                    showFrame={true}
                    scanBarcode={true}
                    laserColor={'red'}
                    frameColor={'#EED064'}
                    colorForScannerFrame={'black'}
                    //Scanner Frame color
                    onReadCode={(event) => this.onReadCode(event.nativeEvent.codeStringValue)}
                />
            </View>
        );
    }
}

export default Scan;