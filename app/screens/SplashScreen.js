import React, { Component } from 'react';
import { View, ImageBackground, StatusBar } from 'react-native';

class SplashScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        setTimeout(() => {
            this.props.navigation.navigate('BottomTabNavigator')
        }, 1000);
    }

    render() {
        return (
            <View>
                <StatusBar
                    translucent
                    backgroundColor='transparent'
                />
                <ImageBackground source={require('../images/Splash-Screen.png')} style={{ width: '100%', height: '100%' }} />
            </View>
        );
    }
}

export default SplashScreen;
