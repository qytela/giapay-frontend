import React, { Component } from 'react'
import { View, Text, Image, Dimensions, ScrollView, StatusBar, SafeAreaView, TouchableOpacity } from 'react-native'
import { Container } from 'native-base'

import { numberFormat }  from '../../helpers'

const { width, height } = Dimensions.get('window')

class TransactionReceived extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {
                ...this.props.navigation.state.params
            }
        }
    }

    render() {
        const { payment, miles } = this.state.data

        return (
            <Container style={{ backgroundColor: '#232323' }}>
                <StatusBar
                    translucent
                    backgroundColor='transparent'
                />
                <SafeAreaView style={{ marginTop: StatusBar.currentHeight }}>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={{ padding: 15 }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={require('../../images/image-giapay-yellow.png')} style={{ width: 130, height: 30 }} />
                                <Image source={require('../../images/image-done.png')} style={{ width: width / 5, height: width / 5, resizeMode: 'contain', marginTop: 50 }} />
                                <Text style={{ fontSize: 20, fontWeight: '500', color: '#EED064', marginTop: 15 }}>Top Up Successful</Text>
                                <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', marginTop: 15 }}>13 February 2020 - 14:00</Text>
                                <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white' }}>Transaction Hash: 0x00000000000</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, marginLeft: 15, marginRight: 15 }}>
                                <Text style={{ fontSize: 15, fontWeight: '500', color: '#EED064' }}>Payment Method</Text>
                                <Text style={{ fontSize: 15, fontWeight: '500', color: 'white' }}>{payment}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15, marginLeft: 15, marginRight: 15 }}>
                                <Text style={{ fontSize: 15, fontWeight: '500', color: '#EED064' }}>Total Payment</Text>
                                <Text style={{ fontSize: 15, fontWeight: '500', color: 'white' }}>Rp {numberFormat(parseInt(miles))}</Text>
                            </View>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('BottomTabNavigator')}
                                activeOpacity={0.8}
                                style={{ width: '100%', height: width / 8, alignItems: 'center', justifyContent: 'center', backgroundColor: '#EED064', borderRadius: 10, marginTop: 100 }}
                            >
                                <Text style={{ fontSize: 15, fontWeight: '500', color: '#232323' }}>Back To Home</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </Container>
        )
    }
}

export default TransactionReceived
