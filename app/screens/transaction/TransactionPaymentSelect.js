import React, { Component } from 'react'
import { View, Text, Image, Dimensions, ScrollView, StatusBar, SafeAreaView, TouchableOpacity } from 'react-native'
import { Container } from 'native-base'
import { RadioButton } from 'react-native-paper'
import { connect } from 'react-redux'
import moment from 'moment-timezone'

import { numberFormat }  from '../../helpers'

const { width, height } = Dimensions.get('window')

class TransactionPaymentSelect extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {
                ...this.props.navigation.state.params
            },
            value: 'BNI'
        }
    }

    onContinuePress = () => {
        const { data, value } = this.state
        const { address, balance } = this.props.account
        const { miles } = data

        const newTransaction = {
            id: this.props.transaction.length > 0 ? parseInt(this.props.transaction[this.props.transaction.length - 1].id) + 1 : 1,
            type: 'received',
            miles: miles,
            receipt: {
                transactionHash: address.slice(0, 20)
            },
            createdAt: moment().tz('Asia/Ujung_Pandang').format()
        }

        this.props.dispatch({ type: 'ADD_TRANSACTION', data: newTransaction })
        this.props.dispatch({ type: 'UPDATE_BALANCE', balance: parseInt(balance) + parseInt(miles) })

        this.props.navigation.navigate({
            key: 'TransactionReceived',
            routeName: 'TransactionReceived',
            params: {
                payment: value,
                miles
            }
        })
    }

    render() {
        const { data, value } = this.state
        const { miles } = data

        return (
            <Container style={{ backgroundColor: '#FFFFFF' }}>
                <StatusBar
                    backgroundColor='#232323'
                />
                <SafeAreaView style={{ marginTop: StatusBar.currentHeight }}>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={{ padding: 15 }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 15, fontWeight: '500', color: '#232323' }}>Total Payment</Text>
                                <Text style={{ fontSize: 25, fontWeight: '500', color: '#232323' }}>Rp {numberFormat(parseInt(miles))}</Text>
                            </View>
                            <Text style={{ fontSize: 15, fontWeight: '500', color: '#232323', marginTop: 50 }}>Choose Payment</Text>
                            <View style={{ marginTop: 15 }}>
                                <RadioButton.Group
                                    value={value}
                                    onValueChange={(value) => this.setState({ value })}
                                >
                                    <View style={{ alignItems: 'center', marginTop: 15, flexDirection: 'row' }}>
                                    <RadioButton
                                        value='BNI'
                                        color='#232323'
                                        uncheckedColor='#232323'
                                    />
                                        <View style={{ width: (width) / 1.2, height: (width) / 8, padding: 15, justifyContent: 'center', borderRadius: 8, borderTopWidth: 2, borderBottomWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderColor: '#EED064' }}>
                                            <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                                                <Text style={{ fontSize: 15,fontWeight: '500', color: '#333' }}>BNI</Text>
                                                <Image source={require('../../images/image-bni.png')} style={{ width: (width) / 5, height: (width) / 15, resizeMode: 'cover' }} />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ alignItems: 'center', marginTop: 15, flexDirection: 'row' }}>
                                    <RadioButton
                                        value='Mandiri'
                                        color='#232323'
                                        uncheckedColor='#232323'
                                    />
                                        <View style={{ width: (width) / 1.2, height: (width) / 8, padding: 15, justifyContent: 'center', borderRadius: 8, borderTopWidth: 2, borderBottomWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderColor: '#EED064' }}>
                                            <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                                                <Text style={{ fontSize: 15,fontWeight: '500', color: '#333' }}>Mandiri</Text>
                                                <Image source={require('../../images/image-mandiri.png')} style={{ width: (width) / 5, height: (width) / 15, resizeMode: 'cover' }} />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ alignItems: 'center', marginTop: 15, flexDirection: 'row' }}>
                                    <RadioButton
                                        value='BCA'
                                        color='#232323'
                                        uncheckedColor='#232323'
                                    />
                                        <View style={{ width: (width) / 1.2, height: (width) / 8, padding: 15, justifyContent: 'center', borderRadius: 8, borderTopWidth: 2, borderBottomWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderColor: '#EED064' }}>
                                            <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                                                <Text style={{ fontSize: 15,fontWeight: '500', color: '#333' }}>BCA</Text>
                                                <Image source={require('../../images/image-bca.png')} style={{ width: (width) / 5, height: (width) / 15, resizeMode: 'cover' }} />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ alignItems: 'center', marginTop: 15, flexDirection: 'row' }}>
                                    <RadioButton
                                        value='Gopay'
                                        color='#232323'
                                        uncheckedColor='#232323'
                                    />
                                        <View style={{ width: (width) / 1.2, height: (width) / 8, padding: 15, justifyContent: 'center', borderRadius: 8, borderTopWidth: 2, borderBottomWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderColor: '#EED064' }}>
                                            <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                                                <Text style={{ fontSize: 15,fontWeight: '500', color: '#333' }}>Gopay</Text>
                                                <Image source={require('../../images/image-gopay.png')} style={{ width: (width) / 5, height: (width) / 15, resizeMode: 'cover' }} />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ alignItems: 'center', marginTop: 15, flexDirection: 'row' }}>
                                    <RadioButton
                                        value='LinkAja'
                                        color='#232323'
                                        uncheckedColor='#232323'
                                    />
                                        <View style={{ width: (width) / 1.2, height: (width) / 8, padding: 15, justifyContent: 'center', borderRadius: 8, borderTopWidth: 2, borderBottomWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderColor: '#EED064' }}>
                                            <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                                                <Text style={{ fontSize: 15,fontWeight: '500', color: '#333' }}>LinkAja</Text>
                                                <Image source={require('../../images/image-linkaja.png')} style={{ width: (width) / 5, height: (width) / 15, resizeMode: 'cover' }} />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ alignItems: 'center', marginTop: 15, flexDirection: 'row' }}>
                                    <RadioButton
                                        value='OVO'
                                        color='#232323'
                                        uncheckedColor='#232323'
                                    />
                                        <View style={{ width: (width) / 1.2, height: (width) / 8, padding: 15, justifyContent: 'center', borderRadius: 8, borderTopWidth: 2, borderBottomWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderColor: '#EED064' }}>
                                            <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>
                                                <Text style={{ fontSize: 15,fontWeight: '500', color: '#333' }}>OVO</Text>
                                                <Image source={require('../../images/image-ovo.png')} style={{ width: (width) / 5, height: (width) / 15, resizeMode: 'cover' }} />
                                            </View>
                                        </View>
                                    </View>
                                </RadioButton.Group>
                            </View>
                            <TouchableOpacity
                                onPress={() => this.onContinuePress()}
                                activeOpacity={0.8}
                                style={{ width: '100%', height: width / 8, alignItems: 'center', justifyContent: 'center', backgroundColor: '#232323', borderRadius: 10, marginTop: 50 }}
                            >
                                <Text style={{ fontSize: 15, fontWeight: '500', color: 'white' }}>Continue</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.goBack()}
                                activeOpacity={0.8}
                                style={{ width: '100%', height: width / 8, alignItems: 'center', justifyContent: 'center', backgroundColor: 'transparent', borderColor: '#232323', borderWidth: 2, borderRadius: 10, marginTop: 15 }}
                            >
                                <Text style={{ fontSize: 15, fontWeight: '500', color: '#232323' }}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        account: state.account,
        transaction: state.transaction
    }
}

export default connect(mapStateToProps)(TransactionPaymentSelect)
