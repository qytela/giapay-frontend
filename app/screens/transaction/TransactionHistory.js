import React, { Component } from 'react'
import { View, Text, Image, Dimensions, ScrollView, StatusBar, SafeAreaView, TouchableOpacity } from 'react-native'
import { Container, Icon } from 'native-base'
import { connect } from 'react-redux'
import moment from 'moment-timezone'

import { numberFormat }  from '../../helpers'

const { width, height } = Dimensions.get('window')

class TransactionHistory extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [
                {
                    id: 0,
                    type: 'sent',
                    miles: 15000,
                    receipt: {
                        transactionHash: `0x${Math.random((Math.floor(99999999) + 0))}`
                    },
                    createdAt: '2020-02-13T14:02:37+08:00'
                },
                {
                    id: 1,
                    type: 'received',
                    miles: 50000,
                    receipt: {
                        transactionHash: `0x${Math.random((Math.floor(99999999) + 0))}`
                    },
                    createdAt: '2020-02-13T14:02:37+08:00'
                },
                {
                    id: 2,
                    type: 'sent',
                    miles: 13000,
                    receipt: {
                        transactionHash: `0x${Math.random((Math.floor(99999999) + 0))}`
                    },
                    createdAt: '2020-02-13T14:02:37+08:00'
                },
                {
                    id: 3,
                    type: 'received',
                    miles: 100000,
                    receipt: {
                        transactionHash: `0x${Math.random((Math.floor(99999999) + 0))}`
                    },
                    createdAt: '2020-02-13T14:02:37+08:00'
                }
            ]
        }
    }

    handleRenderRecentTransaction = (type) => {
        if (type == 'sent') {
            return {
                icon: <Icon type='Feather' name='arrow-up-circle' style={{ fontSize: 40, color: 'red' }} />,
                text: <Text style={{ fontSize: 15, fontWeight: '500', color: 'white', marginLeft: 5 }}>Sent</Text>
            }
        } else if (type == 'received') {
            return {
                icon: <Icon type='Feather' name='arrow-down-circle' style={{ fontSize: 40, color: 'green' }} />,
                text: <Text style={{ fontSize: 15, fontWeight: '500', color: 'white', marginLeft: 5 }}>Received</Text>
            }
        }
    }

    renderRecentTransaction = () => {
        // const { data } = this.state
        const { data } = this.props.transaction

        return data.map((item, index) => {
            const { type, miles, receipt, createdAt } = item
            const { transactionHash } = receipt

            const handle = this.handleRenderRecentTransaction(type)

            return (
                <View key={index} style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 20 }}>
                    <View style={{ flexDirection: 'row' }}>
                        {handle.icon}
                        <View>
                            {handle.text}
                            <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', marginLeft: 5 }}>{transactionHash}</Text>
                        </View>
                    </View>
                    <View style={{ alignItems: 'flex-end' }}>
                        <Text style={{ fontSize: 15, fontWeight: '500', color: 'white', marginLeft: 5 }}>{numberFormat(parseInt(miles))} Miles</Text>
                        <Text style={{ fontSize: 10, fontWeight: 'normal', color: 'white', marginLeft: 5 }}>{moment(createdAt).tz('Asia/Ujung_Pandang').format('DD MMM YYYY HH:mm')}</Text>
                    </View>
                </View>
            )
        })
    }

    render() {
        const { name, balance } = this.props.account

        return (
            <Container style={{ backgroundColor: '#232323' }}>
                <StatusBar
                    translucent
                    backgroundColor='transparent'
                />
                <SafeAreaView style={{ marginTop: StatusBar.currentHeight }}>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={{ width: '100%', height: width / 2, backgroundColor: '#EED064', padding: 15 }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 15 }}>
                                <Text style={{ fontSize: 40, fontWeight: '500', color: '#232323' }}>{numberFormat(parseInt(balance))}</Text>
                                <Text style={{ fontSize: 15, fontWeight: '500', color: '#232323' }}>Miles</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15 }}>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('TopUp')}
                                activeOpacity={0.8}
                                style={{ width: '45%', height: width / 8, alignItems: 'center', justifyContent: 'center', backgroundColor: '#232323', borderRadius: 10 }}
                            >
                                <Text style={{ fontSize: 15, fontWeight: '500', color: 'white' }}>Receive Miles</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('Scan')}
                                activeOpacity={0.8}
                                style={{ width: '45%', height: width / 8, alignItems: 'center', justifyContent: 'center', backgroundColor: '#232323', borderRadius: 10 }}
                            >
                                <Text style={{ fontSize: 15, fontWeight: '500', color: 'white' }}>Send Miles</Text>
                            </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ padding: 15, marginBottom: 30 }}>
                            <Text style={{ fontSize: 15, fontWeight: '500', color: '#EED064' }}>Recent Transaction</Text>
                            {this.renderRecentTransaction()}
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        account: state.account,
        transaction: state.transaction
    }
}

export default connect(mapStateToProps)(TransactionHistory)
