import React, { Component } from 'react'
import { View, Text, Image, TextInput, Dimensions, ScrollView, StatusBar, SafeAreaView, TouchableOpacity } from 'react-native'
import { Container } from 'native-base'
import { connect } from 'react-redux'
import moment from 'moment-timezone'

import { numberFormat }  from '../../helpers'

const { width, height } = Dimensions.get('window')

class TransactionSent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            value: 0
        }
    }

    onConfirmPress = () => {
        const { value } = this.state
        const { balance } = this.props.account

        const newTransaction = {
            id: this.props.transaction.length > 0 ? parseInt(this.props.transaction[this.props.transaction.length - 1].id) + 1 : 1,
            type: 'sent',
            miles: value,
            receipt: {
                transactionHash: '0x86Fa049857E0209aa7D9e616F7eb3b3B78ECfdb0'.slice(0, 20)
            },
            createdAt: moment().tz('Asia/Ujung_Pandang').format()
        }

        this.props.dispatch({ type: 'ADD_TRANSACTION', data: newTransaction })
        this.props.dispatch({ type: 'UPDATE_BALANCE', balance: parseInt(balance) - parseInt(value) })

        alert('Success')

        this.props.navigation.navigate('Home')
    }

    render() {
        const { balance } = this.props.account

        return (
            <Container style={{ backgroundColor: '#232323' }}>
                <StatusBar
                    translucent
                    backgroundColor='transparent'
                />
                <SafeAreaView style={{ marginTop: StatusBar.currentHeight }}>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={{ padding: 15 }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 25, fontWeight: '500', color: 'white' }}>Payment Summary</Text>
                                <Image source={require('../../images/image-b.png')} style={{ width: width / 5, height: width / 5, resizeMode: 'contain', marginTop: 50 }} />
                                <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', marginTop: 15 }}>Batik Keris</Text>
                                <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white' }}>Official Merchan</Text>
                                <Text style={{ fontSize: 20, fontWeight: '500', color: 'white', marginTop: 25 }}>Please Input GarudaMiles</Text>
                                <TextInput
                                    keyboardType='number-pad'
                                    onChangeText={(value) => this.setState({ value })}
                                    style={{ width: '50%', height: width / 8, backgroundColor: 'transparent', padding: 10, borderColor: 'white', borderBottomWidth: 1, borderRadius: 10, marginTop: 10, fontSize: 20, fontWeight: '500', textAlign: 'center', color: 'white' }}
                                />
                            </View>
                            <TouchableOpacity
                                onPress={() => this.onConfirmPress()}
                                activeOpacity={0.8}
                                style={{ width: '100%', height: width / 8, alignItems: 'center', justifyContent: 'center', backgroundColor: '#EED064', borderRadius: 10, marginTop: 100 }}
                            >
                                <Text style={{ fontSize: 15, fontWeight: '500', color: '#232323' }}>Continue Payment</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.goBack()}
                                activeOpacity={0.8}
                                style={{ width: '100%', height: width / 8, alignItems: 'center', justifyContent: 'center', backgroundColor: '#232323', borderColor: '#EED064', borderWidth: 1, borderRadius: 10, marginTop: 15 }}
                            >
                                <Text style={{ fontSize: 15, fontWeight: '500', color: '#EED064' }}>Cancel Payment</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        account: state.account,
        transaction: state.transaction
    }
}

export default connect(mapStateToProps)(TransactionSent)
