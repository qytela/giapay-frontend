import React, { Component } from 'react'
import { View, Text, Image, Dimensions, ScrollView, StatusBar, TouchableOpacity } from 'react-native'
import { Container, Content, Footer, FooterTab, Icon } from 'native-base'
import Modal from 'react-native-modal'
import { connect } from 'react-redux'
import moment from 'moment-timezone'

import { numberFormat }  from '../../helpers'

const { width, height } = Dimensions.get('window')

class ReedemDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modalVisible: false,
            data: this.props.navigation.state.params.data
        }
    }

    onToggleModal = (state) => {
        const { modalVisible, data } = this.state
        const { address, balance } = this.props.account
        const { price } = data

        this.setState({ modalVisible: !modalVisible })

        if (state) {
            const newTransaction = {
                id: this.props.transaction.length > 0 ? parseInt(this.props.transaction[this.props.transaction.length - 1].id) + 1 : 1,
                type: 'sent',
                miles: price,
                receipt: {
                    transactionHash: address.slice(0, 20)
                },
                createdAt: moment().tz('Asia/Ujung_Pandang').format()
            }

            this.props.dispatch({ type: 'ADD_TRANSACTION', data: newTransaction })
            this.props.dispatch({ type: 'UPDATE_BALANCE', balance: parseInt(balance) - parseInt(price) })

            this.props.navigation.navigate({
                key: 'ReedemInfo',
                routeName: 'ReedemInfo',
                params: {
                    data
                }
            })
        }
    }

    renderOverview = () => {
        const { data } = this.state
        const { overview } = data.detail

        return overview.map((item, index) => {
            return (
                <Text key={index} style={{ fontSize: 15, fontWeight: 'normal', color: 'white' }}>{item}</Text>
            )
        })
    }

    renderVenue = () => {
        const { data } = this.state
        const { venue } = data.detail

        return venue.map((item, index) => {
            return (
                <Text key={index} style={{ fontSize: 15, fontWeight: 'normal', color: 'white' }}>{item}</Text>
            )
        })
    }

    renderTC = () => {
        const { data } = this.state
        const { tc } = data.detail

        return tc.map((item, index) => {
            return (
                <Text key={index} style={{ fontSize: 15, fontWeight: 'normal', color: 'white' }}>{item}</Text>
            )
        })
    }

    render() {
        const { modalVisible, data } = this.state
        const { title, image, price } = data
        const { reedemption_period, delivery_period } = data.detail

        return (
            <Container style={{ backgroundColor: '#232323' }}>
                <StatusBar
                    translucent
                    backgroundColor='transparent'
                />
                <Modal
                    isVisible={modalVisible}
                    onSwipeComplete={() => this.onToggleModal()}
                    onBackButtonPress={() => this.onToggleModal()}
                    onBackdropPress={() => this.onToggleModal()}
                >
                    <View style={{ padding: 15, backgroundColor: 'white', borderRadius: 10 }}>
                        <View style={{ alignItems: 'flex-end' }}>
                            <Icon type='Ionicons' name='ios-close-circle' style={{ fontSize: 40, color: '#EA4040' }} onPress={() => this.onToggleModal()} />
                        </View>
                        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ fontSize: 20, fontWeight: '500', color: '#232323', textAlign: 'center' }}>Reedem Miles</Text>
                            <Text style={{ fontSize: 15, fontWeight: 'normal', color: '#232323', textAlign: 'center', marginTop: 5 }}>Are you sure to reedem miles to {title}?</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15 }}>
                            <TouchableOpacity
                                onPress={() => this.onToggleModal()}
                                activeOpacity={0.8}
                                style={{ width: width / 3, height: width / 10, alignItems: 'center', justifyContent: 'center', backgroundColor: 'transparent', borderRadius: 10, borderColor: '#EED064', borderWidth: 2 }}
                            >
                                <Text style={{ fontSize: 15, fontWeight: '500', color: '#EED064' }}>NO, THANKS</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.onToggleModal(true)}
                                activeOpacity={0.8}
                                style={{ width: width / 3, height: width / 10, alignItems: 'center', justifyContent: 'center', backgroundColor: '#EED064', borderRadius: 10 }}
                            >
                                <Text style={{ fontSize: 15, fontWeight: '500', color: '#232323' }}>YES, SURE</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                <Content>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                    >
                        <Image source={{ uri: image }} style={{ width, height: width / 1.5 }} />
                        <View style={{ padding: 15, bottom: 50 }}>
                            <View style={{ width: '100%', height: undefined, backgroundColor: 'white', borderRadius: 10, padding: 15 }}>
                                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 25, fontWeight: '500', color: '#232323', textAlign: 'center' }}>{title}</Text>
                                    <Text style={{ fontSize: 13, fontWeight: 'normal', color: '#232323', textAlign: 'center', marginTop: 10 }}>Reedemption period: {reedemption_period.from} - {reedemption_period.to}</Text>
                                    <Text style={{ fontSize: 20, fontWeight: 'normal', color: '#232323', textAlign: 'center', marginTop: 20 }}>- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 5 }}>
                                    <Text style={{ fontSize: 13, fontWeight: 'normal', color: '#232323', textAlign: 'center' }}>Delivery period</Text>
                                    <Text style={{ fontSize: 13, fontWeight: 'normal', color: '#232323', textAlign: 'center' }}>{delivery_period.from} - {delivery_period.to}</Text>
                                </View>
                            </View>
                            <Text style={{ fontSize: 15, fontWeight: '500', color: '#EED064', marginTop: 15 }}>Overview</Text>
                            {this.renderOverview()}

                            <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', marginTop: 15 }}>Venue:</Text>
                            {this.renderVenue()}

                            <Text style={{ fontSize: 15, fontWeight: '500', color: '#EED064', marginTop: 15 }}>Terms & Conditions</Text>
                            {this.renderTC()}
                        </View>
                    </ScrollView>
                </Content>
                <Footer style={{ height: 80 }}>
                    <FooterTab>
                        <View style={{ width, flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#EED064' }}>
                            <View style={{ marginTop: 15, marginLeft: 15, marginRight: 15 }}>
                                <Text style={{ fontSize: 15, fontWeight: '500', color: '#232323' }}>Prices</Text>
                                <Text style={{ fontSize: 20, fontWeight: '500', color: '#232323' }}>{numberFormat(parseInt(price))} Miles</Text>
                            </View>
                            <View style={{ marginTop: 15, marginLeft: 15, marginRight: 15 }}>
                                <TouchableOpacity
                                    onPress={() => this.onToggleModal()}
                                    activeOpacity={0.8}
                                    style={{ width: width / 3, height: width / 10, alignItems: 'center', justifyContent: 'center', backgroundColor: '#232323', borderRadius: 10 }}
                                >
                                    <Text style={{ fontSize: 15, fontWeight: '500', color: 'white' }}>Reedem Now</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </FooterTab>
                </Footer>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        account: state.account,
        transaction: state.transaction
    }
}

export default connect(mapStateToProps)(ReedemDetail)
