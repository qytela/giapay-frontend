import React, { Component } from 'react'
import { View, Text, Image, Dimensions, ScrollView, StatusBar, SafeAreaView, TouchableOpacity } from 'react-native'
import { Container } from 'native-base'
import QRCode from 'react-native-qrcode-svg'

const { width, height } = Dimensions.get('window')

class ReedemInfo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: this.props.navigation.state.params.data
        }
    }

    render() {
        const { data } = this.state
        const { title } = data

        return (
            <Container style={{ backgroundColor: '#232323' }}>
                <StatusBar
                    translucent
                    backgroundColor='transparent'
                />
                <SafeAreaView style={{ marginTop: StatusBar.currentHeight }}>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={{ padding: 15 }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={require('../../images/image-giapay-yellow.png')} style={{ width: 130, height: 30 }} />
                                <Text style={{ fontSize: 20, fontWeight: '500', color: '#EED064', marginTop: 50 }}>Congratulations</Text>
                                <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', textAlign: 'center', marginTop: 15 }}>Your redeemption has been success. Here's the QR Code, show it our merchant so you can enjoy the {title}.</Text>
                                <View style={{ marginTop: 50 }}>
                                    <QRCode
                                        size={200}
                                        value="0xdaf61829047235a6286dbd15da76792cc12a26b50c620f86c446d413da22c3e8"
                                    />
                                </View>
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate('BottomTabNavigator')}
                                    activeOpacity={0.8}
                                    style={{ width: '100%', height: width / 8, alignItems: 'center', justifyContent: 'center', backgroundColor: '#EED064', borderRadius: 10, marginTop: 100 }}
                                >
                                    <Text style={{ fontSize: 15, fontWeight: '500', color: '#232323' }}>Back To Home</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </Container>
        )
    }
}

export default ReedemInfo
