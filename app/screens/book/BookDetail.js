import React, { Component } from 'react'
import { View, Text, Image, Dimensions, ScrollView, StatusBar, TouchableOpacity } from 'react-native'
import { Container, Content, Footer, FooterTab } from 'native-base'
import { connect } from 'react-redux'
import moment from 'moment-timezone'

import { numberFormat }  from '../../helpers'

const { width, height } = Dimensions.get('window')

class BookDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: this.props.navigation.state.params.data
        }
    }

    onBookNowPress = () => {
        const { price } = this.state.data
        const { address, balance } = this.props.account

        const calculateBalance = parseInt(balance) - parseInt(price) + parseInt(price) * 3
        const getEarn = parseInt(price) * 3

        const newTransactionSent = {
            id: this.props.transaction.length > 0 ? parseInt(this.props.transaction[this.props.transaction.length - 1].id) + 1 : 1,
            type: 'sent',
            miles: price,
            receipt: {
                transactionHash: address.slice(0, 20)
            },
            createdAt: moment().tz('Asia/Ujung_Pandang').format()
        }
        const newTransactionReceived = {
            id: this.props.transaction.length > 0 ? parseInt(this.props.transaction[this.props.transaction.length - 1].id) + 1 : 1,
            type: 'received',
            miles: getEarn,
            receipt: {
                transactionHash: address.slice(0, 20)
            },
            createdAt: moment().tz('Asia/Ujung_Pandang').format()
        }

        this.props.dispatch({ type: 'ADD_TRANSACTION', data: newTransactionSent })
        this.props.dispatch({ type: 'ADD_TRANSACTION', data: newTransactionReceived })
        this.props.dispatch({ type: 'UPDATE_BALANCE', balance: calculateBalance })

        this.props.navigation.navigate({
            key: 'BookNow',
            routeName: 'BookNow',
            params: {
                data: {
                    ...this.state.data,
                    getEarn
                }
            }
        })
    }

    renderOverview = () => {
        const { data } = this.state
        const { overview } = data.detail

        return overview.map((item, index) => {
            return (
                <Text key={index} style={{ fontSize: 15, fontWeight: 'normal', color: 'white' }}>{item}</Text>
            )
        })
    }

    renderTC = () => {
        const { data } = this.state
        const { tc } = data.detail

        return tc.map((item, index) => {
            return (
                <Text key={index} style={{ fontSize: 15, fontWeight: 'normal', color: 'white' }}>{item}</Text>
            )
        })
    }

    render() {
        const { title, image, price } = this.state.data
        const { earn_miles } = this.state.data.detail

        return (
            <Container style={{ backgroundColor: '#232323' }}>
                <StatusBar
                    translucent
                    backgroundColor='transparent'
                />
                <Content>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                    >
                        <Image source={{ uri: image }} style={{ width, height: width / 1.5 }} />
                        <View style={{ padding: 15, bottom: 50 }}>
                            <View style={{ width: '100%', height: undefined, backgroundColor: 'white', borderRadius: 10, padding: 15 }}>
                                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                    <Text style={{ fontSize: 25, fontWeight: '500', color: '#232323', textAlign: 'center' }}>{title}</Text>
                                    <Text style={{ fontSize: 13, fontWeight: 'normal', color: '#232323', textAlign: 'center', marginTop: 10 }}>Booking & Stay Period:</Text>
                                    <Text style={{ fontSize: 13, fontWeight: 'normal', color: '#232323', textAlign: 'center' }}>20 Jan 2020 - 23 Mar 2020</Text>
                                    <Text style={{ fontSize: 20, fontWeight: 'normal', color: '#232323', textAlign: 'center', marginTop: 10 }}>- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 5 }}>
                                    <View>
                                        <Text style={{ fontSize: 13, fontWeight: 'normal', color: '#232323', textAlign: 'center' }}>Weekend</Text>
                                        <Text style={{ fontSize: 13, fontWeight: 'normal', color: '#232323', textAlign: 'center' }}>Weekday</Text>
                                    </View>
                                    <View>
                                        <Text style={{ fontSize: 13, fontWeight: 'normal', color: '#232323', textAlign: 'center' }}>{earn_miles.weekend} Miles</Text>
                                        <Text style={{ fontSize: 13, fontWeight: 'normal', color: '#232323', textAlign: 'center' }}>{earn_miles.weekday} Miles</Text>
                                    </View>
                                </View>
                            </View>
                            <Text style={{ fontSize: 15, fontWeight: '500', color: '#EED064', marginTop: 15 }}>Overview</Text>
                            {this.renderOverview()}

                            <Text style={{ fontSize: 15, fontWeight: '500', color: '#EED064', marginTop: 15 }}>Participating Hotels</Text>

                            <Text style={{ fontSize: 15, fontWeight: '500', color: '#EED064', marginTop: 15 }}>Terms & Conditions</Text>
                            {this.renderTC()}
                        </View>
                    </ScrollView>
                </Content>
                <Footer style={{ height: 80 }}>
                    <FooterTab>
                        <View style={{ width, flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#EED064' }}>
                            <View style={{ marginTop: 15, marginLeft: 15, marginRight: 15 }}>
                                <Text style={{ fontSize: 15, fontWeight: '500', color: '#232323' }}>Earn Up To 3x Miles</Text>
                                <Text style={{ fontSize: 20, fontWeight: '500', color: '#232323' }}>{numberFormat(price)} Miles</Text>
                            </View>
                            <View style={{ marginTop: 15, marginLeft: 15, marginRight: 15 }}>
                                <TouchableOpacity
                                    onPress={() => this.onBookNowPress()}
                                    activeOpacity={0.8}
                                    style={{ width: width / 3, height: width / 10, alignItems: 'center', justifyContent: 'center', backgroundColor: '#232323', borderRadius: 10 }}
                                >
                                    <Text style={{ fontSize: 15, fontWeight: '500', color: 'white' }}>Book Now</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </FooterTab>
                </Footer>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        account: state.account,
        transaction: state.transaction
    }
}

export default connect(mapStateToProps)(BookDetail)
