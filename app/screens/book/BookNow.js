import React, { Component } from 'react'
import { View, Text, Image, TextInput, Dimensions, ScrollView, StatusBar, SafeAreaView, TouchableWithoutFeedback, TouchableOpacity } from 'react-native'
import { Container } from 'native-base'
import DateTimePicker from 'react-native-modal-datetime-picker'
import moment from 'moment-timezone'

const { width, height } = Dimensions.get('window')

class BookNow extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: this.props.navigation.state.params.data,
            isCheckInVisible: false,
            isCheckOutVisible: false,
            checkIn: '',
            checkOut: '',
            rooms: 0,
            adults: 0,
            childrens: 0,
            member_card: ''
        } 
    }

    handleCheckIn = (date = null, state = false) => {
        const { isCheckInVisible, checkIn } = this.state

        this.setState({ isCheckInVisible: !isCheckInVisible })

        if (date !== null && state) {
            this.setState({
                isCheckInVisible: !isCheckInVisible,
                checkIn: moment(date).format('DD MMMM YYYY')
            })
        }
    }

    handleCheckOut = (date = null, state = false) => {
        const { isCheckOutVisible, checkOut } = this.state

        this.setState({ isCheckOutVisible: !isCheckOutVisible })

        if (date !== null && state) {
            this.setState({
                isCheckOutVisible: !isCheckOutVisible,
                checkOut: moment(date).format('DD MMMM YYYY')
            })
        }
    }

    onBookNowPress = () => {
        const { checkIn, checkOut, rooms, adults, childrens, member_card } = this.state
        const { getEarn } = this.state.data

        this.props.navigation.navigate({
            key: 'BookInfo',
            routeName: 'BookInfo',
            params: {
                data: {
                    checkIn,
                    checkOut,
                    rooms,
                    adults,
                    childrens,
                    member_card,
                    getEarn
                }
            }
        })
    }

    render() {
        const { checkIn, checkOut } = this.state
        const { title } = this.state.data

        return (
            <Container style={{ backgroundColor: '#232323' }}>
                <StatusBar
                    translucent
                    backgroundColor='transparent'
                />
                <SafeAreaView style={{ marginTop: StatusBar.currentHeight }}>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={{ padding: 15 }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <Image source={require('../../images/image-giapay-yellow.png')} style={{ width: 130, height: 30 }} />
                            </View>
                            <View style={{ alignItems: 'baseline', marginTop: 50 }}>
                                <Text style={{ fontSize: 25, fontWeight: '500', color: '#EED064', marginTop: 5 }}>Hi, Richard</Text>
                                <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', marginTop: 15 }}>Destination Hotel</Text>
                                <View style={{ width: '100%', height: width / 8, padding: 15, justifyContent: 'center', backgroundColor: 'white', borderRadius: 10, marginTop: 5, fontSize: 20, fontWeight: '500', color: '#232323' }}>
                                    <Text style={{ fontSize: 15, fontWeight: '500', color: '#232323' }}>{title}</Text>
                                </View>

                                <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', marginTop: 15 }}>Check In Date</Text>
                                <TouchableWithoutFeedback activeOpacity={1} onPress={() => this.handleCheckIn(null, true)} style={{ width: '100%', height: width / 8 }}>
                                    <View style={{ width: '100%', height: width / 8, padding: 15, justifyContent: 'center', backgroundColor: 'white', borderRadius: 10, marginTop: 5, fontSize: 20, fontWeight: '500', color: '#232323' }}>
                                        <Text style={{ fontSize: 15, fontWeight: '500', color: '#232323' }}>{checkIn}</Text>
                                    </View>
                                </TouchableWithoutFeedback>
                                <DateTimePicker
                                    isVisible={this.state.isCheckInVisible}
                                    mode='date'
                                    onConfirm={(date) => this.handleCheckIn(date, true)}
                                    onCancel={() => this.handleCheckIn()}
                                />

                                <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', marginTop: 15 }}>Check Out Date</Text>
                                <TouchableWithoutFeedback activeOpacity={1} onPress={() => this.handleCheckOut(null, true)} style={{ width: '100%', height: width / 8 }}>
                                    <View style={{ width: '100%', height: width / 8, padding: 15, justifyContent: 'center', backgroundColor: 'white', borderRadius: 10, marginTop: 5, fontSize: 20, fontWeight: '500', color: '#232323' }}>
                                        <Text style={{ fontSize: 15, fontWeight: '500', color: '#232323' }}>{checkOut}</Text>
                                    </View>
                                </TouchableWithoutFeedback>
                                <DateTimePicker
                                    isVisible={this.state.isCheckOutVisible}
                                    mode='date'
                                    onConfirm={(date) => this.handleCheckOut(date, true)}
                                    onCancel={() => this.handleCheckOut()}
                                />

                                <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', marginTop: 15 }}>Room(s)</Text>
                                <TextInput
                                    keyboardType='number-pad'
                                    onChangeText={(rooms) => this.setState({ rooms })}
                                    style={{ width: '100%', height: width / 8, backgroundColor: 'white', borderRadius: 10, marginTop: 5, fontSize: 20, fontWeight: '500', color: '#232323' }}
                                />

                                <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', marginTop: 15 }}>Adult(s)</Text>
                                <TextInput
                                    keyboardType='number-pad'
                                    onChangeText={(adults) => this.setState({ adults })}
                                    style={{ width: '100%', height: width / 8, backgroundColor: 'white', borderRadius: 10, marginTop: 5, fontSize: 20, fontWeight: '500', color: '#232323' }}
                                />

                                <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', marginTop: 15 }}>Childrens(s)</Text>
                                <TextInput
                                    keyboardType='number-pad'
                                    onChangeText={(childrens) => this.setState({ childrens })}
                                    style={{ width: '100%', height: width / 8, backgroundColor: 'white', borderRadius: 10, marginTop: 5, fontSize: 20, fontWeight: '500', color: '#232323' }}
                                />

                                <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', marginTop: 15 }}>Loyalty or membership card number</Text>
                                <TextInput
                                    keyboardType='number-pad'
                                    onChangeText={(member_card) => this.setState({ member_card })}
                                    style={{ width: '100%', height: width / 8, backgroundColor: 'white', borderRadius: 10, marginTop: 5, fontSize: 20, fontWeight: '500', color: '#232323' }}
                                />
                            </View>
                            <TouchableOpacity
                                onPress={() => this.onBookNowPress()}
                                activeOpacity={0.8}
                                style={{ width: '100%', height: width / 8, alignItems: 'center', justifyContent: 'center', backgroundColor: '#EED064', borderRadius: 10, marginTop: 50, marginBottom: 30 }}
                            >
                                <Text style={{ fontSize: 15, fontWeight: '500', color: '#232323' }}>Book and Earn Miles</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </Container>
        )
    }
}

export default BookNow
