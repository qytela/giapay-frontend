import React, { Component } from 'react'
import { View, Text, Image, Dimensions, ScrollView, StatusBar, SafeAreaView, TouchableOpacity } from 'react-native'
import { Container } from 'native-base'
import QRCode from 'react-native-qrcode-svg'

import { numberFormat }  from '../../helpers'

const { width, height } = Dimensions.get('window')

class BookInfo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: this.props.navigation.state.params.data
        }
    }

    renderRoom = () => {
        const { rooms, adults, childrens } = this.state.data

        const text = `${rooms || 0} Room, ${adults || 0} Adult, ${childrens || 0} Children`

        return (
            <Text style={{ color: 'white' }}>{text}</Text>
        )
    }

    render() {
        const { checkIn, checkOut, getEarn } = this.state.data

        return (
            <Container style={{ backgroundColor: '#232323' }}>
                <StatusBar
                    translucent
                    backgroundColor='transparent'
                />
                <SafeAreaView style={{ marginTop: StatusBar.currentHeight }}>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={{ padding: 15 }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={require('../../images/image-giapay-yellow.png')} style={{ width: 130, height: 30 }} />
                                <Text style={{ fontSize: 20, fontWeight: '500', color: '#EED064', marginTop: 50 }}>Your Ticket Details</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15, marginLeft: 50, marginRight: 50 }}>
                                <Text style={{ color: 'white' }}>Check In</Text>
                                <Text style={{ color: 'white' }}>{checkIn}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15, marginLeft: 50, marginRight: 50 }}>
                                <Text style={{ color: 'white' }}>Check Out</Text>
                                <Text style={{ color: 'white' }}>{checkOut}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 15, marginLeft: 50, marginRight: 50 }}>
                                <Text style={{ color: 'white' }}>Room</Text>
                                {this.renderRoom()}
                            </View>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <View style={{ marginTop: 50 }}>
                                    <QRCode
                                        size={200}
                                        value="0xdaf61829047235a6286dbd15da76792cc12a26b50c620f86c446d413da22c3e8"
                                    />
                                </View>
                            </View>
                            <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 50 }}>
                            <Text style={{ fontSize: 20, fontWeight: '500', color: '#EED064' }}>Congratulations</Text>
                            <Text style={{ fontSize: 15, fontWeight: 'normal', color: 'white', textAlign: 'center' }}>Your booking has been success. You just earn {numberFormat(parseInt(getEarn))} Miles. Here's the QR Code, show it to our merchant so you can enjot a night at hotel.</Text>
                            </View>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('BottomTabNavigator')}
                                activeOpacity={0.8}
                                style={{ width: '100%', height: width / 8, alignItems: 'center', justifyContent: 'center', backgroundColor: '#EED064', borderRadius: 10, marginTop: 100 }}
                            >
                                <Text style={{ fontSize: 15, fontWeight: '500', color: '#232323' }}>Back To Home</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </Container>
        )
    }
}

export default BookInfo
