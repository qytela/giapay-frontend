import React from 'react'
import { View } from 'react-native'
import { Icon } from 'native-base'
import { createAppContainer, createBottomTabNavigator, createStackNavigator } from 'react-navigation'
import { Provider } from 'react-redux'

import store from './app/redux/store'

import SplashScreen from './app/screens/SplashScreen'

import Home from './app/screens/bottomtab/Home'
import Scan from './app/screens/bottomtab/Scan'
import TopUp from './app/screens/bottomtab/TopUp'

import BookDetail from './app/screens/book/BookDetail'
import BookNow from './app/screens/book/BookNow'
import BookInfo from './app/screens/book/BookInfo'

import ReedemDetail from './app/screens/reedem/ReedemDetail'
import ReedemInfo from './app/screens/reedem/ReedemInfo'

import TransactionHistory from './app/screens/transaction/TransactionHistory'
import TransactionPaymentSelect from './app/screens/transaction/TransactionPaymentSelect'
import TransactionReceived from './app/screens/transaction/TransactionReceived'
import TransactionSent from './app/screens/transaction/TransactionSent'

const BottomTabNavigator = createBottomTabNavigator({
  Home: {
    screen: Home,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <Icon type='AntDesign' name='home' style={{ fontSize: 24, color: tintColor }} />
      )
    },
  },
  History: {
    screen: TransactionHistory,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <Icon type='Octicons' name='history' style={{ fontSize: 24, color: tintColor }} />
      )
    }
  },
  Scan: {
    screen: Scan,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <View style={{ width: 60, height: 60, borderRadius: 30, alignItems: 'center', justifyContent: 'center', backgroundColor: '#333', bottom: 20, borderColor: '#EED064', borderWidth: 5 }}>
          <Icon type='AntDesign' name='qrcode' style={{ fontSize: 24, color: '#EED064' }} />
        </View>
      )
    }
  },
  TopUp: {
    screen: TopUp,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <Icon type='Entypo' name='wallet' style={{ fontSize: 24, color: tintColor }} />
      )
    }
  },
  Profile: {
    screen: Home,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <Icon type='Octicons' name='person' style={{ fontSize: 24, color: tintColor }} />
      )
    }
  }
}, {
  initialRouteName: 'Home',
  tabBarOptions: {
    labelStyle: {
      fontWeight: '500'
    },
    style: {
      height: 60,
      backgroundColor: '#EED064'
    },
    activeTintColor: '#333',
    inactiveTintColor: 'grey'
  }
})

const StackNavigator = createStackNavigator({
  SplashScreen,
  BottomTabNavigator,
  BookDetail,
  BookNow,
  BookInfo,
  ReedemDetail,
  ReedemInfo,
  TransactionHistory,
  TransactionPaymentSelect,
  TransactionReceived,
  TransactionSent
}, {
  headerMode: 'none',
  initialRouteName: 'SplashScreen'
})

const AppContainer = createAppContainer(StackNavigator)

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    )
  }
}